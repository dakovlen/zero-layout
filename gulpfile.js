let projectFolder = 'dist';
let sourceFolder = 'src';

let path = {
  build: {
    html: projectFolder + '/',
    css: projectFolder + '/css/',
    js: projectFolder + '/js/',
    images: projectFolder + '/images',
    fonts: projectFolder + '/fonts'
  },
  src: {
    html: sourceFolder + '/*.html ',
    css: sourceFolder + '/scss/style.scss',
    js: sourceFolder + '/js/script.js',
    images: sourceFolder + '/images/**/*.{jpg,png,svg,gif,ico,webp}',
    fonts: sourceFolder + '/fonts/**/*.{eot,svg,ttf,woff,woff2}'
  },
  watch: {
    html: sourceFolder + '/**/*.html',
    css: sourceFolder + '/scss/**/*.scss',
    js: sourceFolder + '/js/**/*.js',
    images: sourceFolder + '/images/**/*.{jpg,png,svg,gif,ico,webp}'
  },
  clean: './' + projectFolder + '/'
}

let { src, dest } = require('gulp'),
  gulp = require('gulp'),
  browsersync = require('browser-sync').create(),
  fileinclude = require('gulp-file-include'),
  del = require('del'),
  scss = require('gulp-sass'),
  autoprefixer = require('gulp-autoprefixer'),
  clean_css = require('gulp-clean-css'),
  rename = require('gulp-rename'),
  group_media = require('gulp-group-css-media-queries'),
  uglify = require('gulp-uglify-es').default,
  imagemin = require('gulp-imagemin')

function browserSync(params) {
  browsersync.init({
    server: {
      baseDir: './' + projectFolder + '/'
    },
    port: 3000,
    notify: false
  })
}

function html() {
  return src(path.src.html)
    .pipe(fileinclude())
    .pipe(dest(path.build.html))
    .pipe(browsersync.stream())
}

function clean(params) {
  return del(path.clean)
}

function css() {
  return src(path.src.css)
    .pipe(
      scss({
        outputStyle: 'expanded'
      })
    )
    .pipe(
      group_media()
    )
    .pipe(dest(path.build.css))
    .pipe(
      clean_css()
    )
    .pipe(autoprefixer({
      overrideBrowserslist: ['last 5 versions'],
      cascade: true
    }))
    .pipe(
      rename({
        extname: '.min.css'
      }
      )
    )

    .pipe(dest(path.build.css))
    .pipe(browsersync.stream())
}

function js() {
  return src(path.src.js)
    .pipe(fileinclude())
    .pipe(dest(path.build.js))
    .pipe(
      uglify()
    )
    .pipe(
      rename({
        extname: '.min.js'
      }
      )
    )
    .pipe(dest(path.build.js))
    .pipe(browsersync.stream())
}
function images() {
  return src(path.src.images)
    .pipe(imagemin({
      interlaced: true,
      progressive: true,
      optimizationLevel: 3,
      svgoPlugins: [
        {
          removeViewBox: false
        }
      ]
    }))
    .pipe(dest(path.build.images))
    .pipe(browsersync.stream())
}
function fonts() {
  return src(path.src.fonts)
    .pipe(dest(path.build.fonts))
    .pipe(browsersync.stream())
}


function watchFiles(params) {
  gulp.watch([path.watch.html], html);
  gulp.watch([path.watch.css], css);
  gulp.watch([path.watch.js], js);
  gulp.watch([path.watch.images], images);
}

let build = gulp.series(clean, gulp.parallel(css, js, html, images, fonts));
let watch = gulp.parallel(build, watchFiles, browserSync);

exports.fonts = fonts;
exports.images = images;
exports.js = js;
exports.css = css;
exports.html = html;
exports.build = build;
exports.watch = watch;
exports.default = watch